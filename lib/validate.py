#!/usr/bin/env python
# encoding: utf-8

from couchdb.crypt import check_signature
from couchdb import db

def fun(newDoc, oldDoc, userCtx, security={}):
	if 'deleted' in newDoc and newDoc['deleted']:
		return 1

	if newDoc['signed']:
		return 1
	return {"forbidden": "WTF is here?"}


