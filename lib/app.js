// lib/app.js

exports.views = {
  by_type: {
    map: function (doc) {
      if (doc.content && doc.content.type) {
	emit(doc.content.type, doc);
      }
    }
  },

  claims: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "claim")
	emit(doc.content.permanode, doc);
    }
  },
  
  claims_ext: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "claim") {
        var i = 0, _l;

        var val = {
          type: doc.content.claimType,
          value: doc.content.sub.value,
          date: new Date(doc.content.date),
          permanode: doc.content.permanode
        };

        if (doc.content.sub && doc.content.sub.attribute) {
          emit([ "permanode",
                 doc.content.permanode,
                 doc.content.sub.attribute,
                 doc.content.date,
                 doc.content.sub.value],
              doc);

          emit([ "attribute",
                 doc.content.sub.attribute,
                 doc.content.sub.value,
                 doc.content.permanode,
                 doc.content.date],
               doc);
        }
      }
    },

    reduce: function (keys, values, rereduce) {

      if (!rereduce) {
        values = values.map(function (item) {
                   if (item.content.claimType === "del-attribute") {
                     return { past: item, cur: [] };
                   } else {
                     return { past: [], cur: item };
                   }
                 });
      }

      if (values instanceof Array || (typeof values.reduce === "function")) {
        var concat_all = function (arr, key) {
          return arr.reduce (function (cur, i) {
                   return cur.concat(i[key]);
                 }, []);
        };

        values = { past: concat_all(values, 'past'), cur: concat_all(values, 'cur') };
      }

      if (!(values.cur instanceof Array)) {
        values.cur = [];
      }


      /* Get the latest "set" attribute, and remove everything before,
       * as it if was a "del"
       */
      var all_set = values.cur.filter(function (item) {
                      return item.content.claimType === "set-attribute";
                    });

      var latest_set = all_set.reduce (function (cur, val) {
                         if (cur.content.date === null)
                           return val;

                         return cur.content.date < val.content.date ? val : cur;
                       }, { content: { date: null }});

      var last_generation = values.cur.filter (function (item) {
                              return item.content.date < latest_set.content.date
                                  || values.past.some (function (past) {
                                       return item.content.date < past.content.date;
                                     });
                            });

      var by_date = function (a, b) {
        return a.content.date < b.content.date
             ? -1
             : (a.content.date == b.content.date ? 0 : 1);
      };

      values.past = values.past.concat (last_generation).sort (by_date);

      /* Recreate cur without values in past */
      var new_generation = values.cur.filter (function (item) {
                             return values.past.indexOf(item) == -1;
                           });

      values.cur = new_generation.sort (by_date);

      return values;
    }
  },

  claims_by_attribute: {
    map: function (doc) {
      if (doc.content  && doc.content.type && doc.content.type === "claim") {
        if (doc.content.sub.attribute) {
          emit ([ "permanode",
                  doc.content.permanode,
                  doc.content.sub.attribute,
                  doc.content.date
                ],
                {
                  type: doc.content.claimType,
                  date: doc.content.date,
                  value: doc.content.sub.value
                });
        }
      }
    },
    reduce: function (keys, values, rereduce) {
            
      values = values.sort (function (a, b) {
        if (a.date == b.date) return 0;
        
        return a.date > b.date ? 1 : -1;
      });
      
      return values.slice(1).reduce (function (cur, val, i) {
               var v;
               
               if (val.type == "set-attribute")
                 return val.value;
               
               if (val.type == "add-attribute") {
                 if (cur != null && cur instanceof Array) {
                   cur.push (val.value);
                   return cur;
                 } else {
                   return [cur, val.value];
                 }
               }

               if (val.type == "del-attribute") {
                 return undefined;
               }
               return { cur: cur, val: val };
      }, values[0].value);
    }
  },

  by_name: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "claim")
	if (doc.content.claimType === "set-attribute" && doc.content.sub.attribute === "title")
	  emit (doc.content.sub.value, doc);
    }
  },
  
  id_all: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "identity")
	emit (doc._id, doc);
    }
  },
  
  id_signable: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "identity")
	if (doc.content.privKey)
	  emit (doc._id, doc);
    }
  },
  
  
  claimsForBlob: {
    map: function (doc) {
      if (doc.content && doc.content.type && doc.content.type === "claim")
	// TODO: Model blobs
	if (true) {
	  emit(doc.key, doc);
	}
    }
  },
  
  by_type: {
    map: function (doc, e) {
      if (doc.content && doc.content.type) {
        emit (doc.content.type, doc);
      }
    }
  }
};

